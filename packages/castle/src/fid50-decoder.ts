// Hey Emacs, this is -*- coding: utf-8 -*-

import { Transform, TransformOptions, TransformCallback } from 'stream';

import type { DeepWriteable } from '~/lib/types';
import { FID50_ESN, FID50_ESN_LOW, MhPacket } from '~/fid50-packets';

// export type MhPackage = ReturnType<InstanceType<typeof MhPacket>['toJSON']>;

export const mhPackageInitial = {
  esn: 250,
  esnLow: 250,
  esnHigh: 0,
  gasPressureFaults: {
    fuel: 0,
    air: 0,
    spanA: 0,
    spanB: 0,
    purge: 0,
    zero: 0,
    vacuum: 0,
    _value: 0,
  },
  probeAndHeadTcFaults: { probe1: 0, probe2: 0, head1: 0, head2: 0, _value: 0 },
  exhaustTcFaults: { exhaust1: 0, exhaust2: 0, safeState: 0, _value: 0 },
  ch1SolenoidsFaults: {
    fuel: 0,
    air: 0,
    spanA: 0,
    spanB: 0,
    purge: 0,
    zero: 0,
    vacuum: 0,
    test: 0,
    _value: 0,
  },
  ch2SolenoidsFaults: {
    fuel: 0,
    air: 0,
    spanA: 0,
    spanB: 0,
    purge: 0,
    zero: 0,
    vacuum: 0,
    test: 0,
    _value: 0,
  },
  userDigitalInputs: {
    isolated0: 0,
    isolated1: 0,
    isolated2: 0,
    isolated3: 0,
    nonIsolated0: 0,
    nonIsolated1: 0,
    nonIsolated2: 0,
    nonIsolated3: 0,
    nonIsolatedValue: 0,
    isolatedValue: 0,
    _value: 0,
  },
  ch1ExhaustTemperature: 0,
  ch1HeadTemperature: 0,
  ch1OutputPpm: 0,
  ch1CpPressure: 0,
  ch1FidPressure: 0,
  ch1CpBleed: 0,
  ch1FidBleed: 0,
  ch1GlowPlugCurrent: 0,
  ch2ExhaustTemperature: 0,
  ch2HeadTemperature: 0,
  ch2OutputV: 0,
  pumpRpm: 0,
  fanRpm: 0,
  ch2CpBleed: 0,
  ch2FidBleed: 0,
  ch2GlowPlugCurrent: 0,
  ch1ProbeTemperature: 0,
  ch2ProbeTemperature: 0,
  ch1FuelPressure: 0,
  ch1FuelBleed: 0,
  ch1AirPressure: 0,
  ch1AirBleed: 0,
  zeroOffset: 0,
  maxFactor: 0,
  ambientTemperature: 0,
  ch1ProbeDrivePercent: 0,
  ch2ProbeDrivePercent: 0,
  ch1HeadDrivePercent: 0,
  mainsFrequency: 0,
  outputPpm: 0,
  mainPcbUpTimeSecond: 0,
  pumpPcbUpTimeSecond: 0,
  pumpTotalRunningTimeMinute: 0,
  firmwareVersionMajor: 0,
  firmwareVersionMinor: 0,
};

export type MhPackage = DeepWriteable<typeof mhPackageInitial>;

const mhPacketRawLength = MhPacket.baseSize;

export class Decoder extends Transform {
  constructor(options?: TransformOptions) {
    super({
      ...options,
      readableObjectMode: true,
    });
  }

  _transform(
    chunk: Buffer,
    _encoding: BufferEncoding,
    callback: TransformCallback,
  ): void {
    chunk.forEach((value) => {
      this._mhPacketRaw[this._mhPacketRawIndex] = value;
      this._mhPacketRawIndex += 1;

      // Drop packages with incorrect ESN
      if (this._mhPacketRawIndex === 1) {
        if (this._mhPacket.esnLow !== FID50_ESN_LOW) {
          // TODO: Report package lost error
          this._mhPacketRawIndex = 0;
        }
      } else if (this._mhPacketRawIndex === 2) {
        if (this._mhPacket.esn !== FID50_ESN) {
          // TODO: Report package lost error
          this._mhPacketRawIndex = 0;
        }
      }
    });

    if (this._mhPacketRawIndex === mhPacketRawLength) {
      this._mhPacketRawIndex = 0;
      this.transformAndPush();
    }

    callback();
  }

  _flush(callback: TransformCallback): void {
    this._mhPacketRawIndex = 0;
    callback();
  }

  private transformAndPush(): void {
    // TODO: transform JSON object instead of just forwarding it
    const mhPackage: MhPackage = new MhPacket(
      this._mhPacketRaw.slice(),
    ).toJSON();

    this.push(mhPackage);
  }

  private readonly _mhPacket = new MhPacket();
  private readonly _mhPacketRaw = MhPacket.raw(this._mhPacket);
  private _mhPacketRawIndex = 0;
}
