// Hey Emacs, this is -*- coding: utf-8 -*-

/* eslint-disable no-bitwise */

import Struct from 'typed-struct';

export const FID50_ESN_LOW = 250;
export const FID50_ESN_HIGH = 0;

export const FID50_ESN = (FID50_ESN_HIGH << 8) + FID50_ESN_LOW;

/// /b/; MhPacket (Module-to-Host Packet)
/// /b/{

export const MhPacket = new Struct('MhPacket')
  .UInt16LE('esn', FID50_ESN)
  .back()
  .UInt8('esnLow')
  .UInt8('esnHigh')
  .Struct(
    'gasPressureFaults',
    new Struct('GasPressureFaults')
      .Bits8({
        fuel: [7 - 0, 1],
        air: [7 - 1, 1],
        spanA: [7 - 2, 1],
        spanB: [7 - 3, 1],
        purge: [7 - 4, 1],
        zero: [7 - 5, 1],
        vacuum: [7 - 6, 1],
        _value: [0, 8],
      })
      .compile(),
  )
  .Struct(
    'probeAndHeadTcFaults',
    new Struct('ProbeAndHeadTcFaults')
      .Bits8({
        probe1: [7 - 2, 1],
        probe2: [7 - 3, 1],
        head1: [7 - 6, 1],
        head2: [7 - 7, 1],
        _value: [0, 8],
      })
      .compile(),
  )
  .Struct(
    'exhaustTcFaults',
    new Struct('ExhaustTcFaults')
      .Bits8({
        exhaust1: [7 - 0, 1],
        exhaust2: [7 - 1, 1],
        safeState: [7 - 7, 1],
        _value: [0, 8],
      })
      .compile(),
  )
  .Struct(
    'ch1SolenoidsFaults',
    new Struct('Ch1SolenoidsFaults')
      .Bits8({
        fuel: [7 - 0, 1],
        air: [7 - 1, 1],
        spanA: [7 - 2, 1],
        spanB: [7 - 3, 1],
        purge: [7 - 4, 1],
        zero: [7 - 5, 1],
        vacuum: [7 - 6, 1],
        test: [7 - 7, 1],
        _value: [0, 8],
      })
      .compile(),
  )
  .Struct(
    'ch2SolenoidsFaults',
    new Struct('Ch2SolenoidsFaults')
      .Bits8({
        fuel: [7 - 0, 1],
        air: [7 - 1, 1],
        spanA: [7 - 2, 1],
        spanB: [7 - 3, 1],
        purge: [7 - 4, 1],
        zero: [7 - 5, 1],
        vacuum: [7 - 6, 1],
        test: [7 - 7, 1],
        _value: [0, 8],
      })
      .compile(),
  )
  .Struct(
    'userDigitalInputs',
    new Struct('UserDigitalInputs')
      .Bits8({
        isolated0: [7 - 0, 1],
        isolated1: [7 - 1, 1],
        isolated2: [7 - 2, 1],
        isolated3: [7 - 3, 1],
        nonIsolated0: [7 - 4, 1],
        nonIsolated1: [7 - 5, 1],
        nonIsolated2: [7 - 6, 1],
        nonIsolated3: [7 - 7, 1],
        nonIsolatedValue: [7 - 7, 4],
        isolatedValue: [7 - 3, 4],
        _value: [0, 8],
      })
      .compile(),
  )
  .UInt16BE('ch1ExhaustTemperature')
  .UInt16BE('ch1HeadTemperature')
  .UInt16BE('ch1OutputPpm')
  .UInt16BE('ch1CpPressure')
  .UInt16BE('ch1FidPressure')
  .UInt16BE('ch1CpBleed')
  .UInt16BE('ch1FidBleed')
  .UInt16BE('ch1GlowPlugCurrent')
  .UInt16BE('ch2ExhaustTemperature')
  .UInt16BE('ch2HeadTemperature')
  .UInt16BE('ch2OutputV')
  .UInt16BE('pumpRpm')
  .UInt16BE('fanRpm')
  .UInt16BE('ch2CpBleed')
  .UInt16BE('ch2FidBleed')
  .UInt16BE('ch2GlowPlugCurrent')
  .UInt16BE('ch1ProbeTemperature')
  .UInt16BE('ch2ProbeTemperature')
  .UInt16BE('ch1FuelPressure')
  .UInt16BE('ch1FuelBleed')
  .UInt16BE('ch1AirPressure')
  .UInt16BE('ch1AirBleed')
  .UInt16BE('zeroOffset')
  .UInt16BE('maxFactor')
  .UInt16BE('ambientTemperature')
  .UInt16BE('ch1ProbeDrivePercent')
  .UInt16BE('ch2ProbeDrivePercent')
  .UInt16BE('ch1HeadDrivePercent')
  .seek(1)
  .UInt8('mainsFrequency')
  .Float32LE('outputPpm')
  .UInt16BE('mainPcbUpTimeSecond')
  .UInt16BE('pumpPcbUpTimeSecond')
  .Float32LE('pumpTotalRunningTimeMinute')
  .UInt8('firmwareVersionMajor')
  .UInt8('firmwareVersionMinor')
  .compile();

/// /b/}

/// /b/; HmPacket (Host-to-Module Packet)
/// /b/{

export const HmPacket = new Struct('HmPacket')
  .UInt16LE('esn', FID50_ESN)
  .Struct(
    'ch1Solenoids',
    new Struct('Ch1Solenoids')
      .Bits8({
        fuel: [7 - 0, 1],
        air: [7 - 1, 1],
        spanA: [7 - 2, 1],
        spanB: [7 - 3, 1],
        purge: [7 - 4, 1],
        zero: [7 - 5, 1],
        vacuum: [7 - 6, 1],
        test: [7 - 7, 1],
        _value: [0, 8],
      })
      .compile(),
  )
  .Struct(
    'ch2Solenoids',
    new Struct('Ch2Solenoids')
      .Bits8({
        fuel: [7 - 0, 1],
        air: [7 - 1, 1],
        spanA: [7 - 2, 1],
        spanB: [7 - 3, 1],
        purge: [7 - 4, 1],
        zero: [7 - 5, 1],
        vacuum: [7 - 6, 1],
        test: [7 - 7, 1],
        _value: [0, 8],
      })
      .compile(),
  )
  .Struct(
    'lhcGlowPlugAndHeadHeaterStatus',
    new Struct('LhcGlowPlugAndHeadHeaterStatus')
      .Bits8({
        headHeater1: [7 - 0, 1],
        headHeater2: [7 - 1, 1],
        glowPlug1: [7 - 2, 1],
        glowPlug2: [7 - 3, 1],
        lhc1: [7 - 4, 1],
        lhc2: [7 - 5, 1],
        _value: [0, 8],
      })
      .compile(),
  )
  .Struct(
    'gainRangeAndHtStatus',
    new Struct('GainRangeAndHtStatus')
      .Bits8({
        ht1: [7 - 0, 1],
        ch1AmplificationStage: [7 - 3, 3],
        ht2: [7 - 4, 1],
        ch2AmplificationStage: [7 - 7, 3],
        _value: [0, 8],
      })
      .compile(),
  )
  .Struct(
    'userDigitalOutput',
    new Struct('UserDigitalOutput')
      .Bits8({
        isolated0: [7 - 0, 1],
        isolated1: [7 - 1, 1],
        isolated2: [7 - 2, 1],
        isolated3: [7 - 3, 1],
        nonIsolated0: [7 - 4, 1],
        nonIsolated1: [7 - 5, 1],
        nonIsolated2: [7 - 6, 1],
        nonIsolated3: [7 - 7, 1],
        nonIsolatedValue: [7 - 7, 4],
        isolatedValue: [7 - 3, 4],
        _value: [0, 8],
      })
      .compile(),
  )
  .UInt8('ch1FidPressureSetpoint')
  .UInt8('ch1CpPressureSetpoint')
  .UInt16BE('pumpRpmSetpoint')
  .UInt8('ch1FuelPressureSetpoint')
  .UInt8('ch1AirPressureSetpoint')
  .UInt8('glowplugCurrent')
  .UInt8('ch2AirPressureSetpoint')
  .UInt8('ch1ProbeTemperatureSetpoint')
  .UInt8('ch1HeadTemperatureSetpoint')
  .UInt8('ch2ProbeTemperatureSetpoint')
  .UInt8('ch2HeadTemperatureSetpoint')
  .UInt16BE('span1')
  .UInt16BE('zero1')
  .UInt16BE('span2')
  .UInt16BE('zero2')
  .compile();

/// /b/}
