// Hey Emacs, this is -*- coding: utf-8 -*-

import { Transform, TransformOptions, TransformCallback } from 'stream';

import merge from 'lodash.merge';

import type { DeepWriteable } from '~/lib/types';
import { FID50_ESN, HmPacket } from '~/fid50-packets';

// export type HmPackage = ReturnType<InstanceType<typeof HmPacket>['toJSON']>;

export const hmPackageInitial = {
  esn: 250,
  ch1Solenoids: {
    fuel: 0,
    air: 0,
    spanA: 0,
    spanB: 0,
    purge: 0,
    zero: 0,
    vacuum: 0,
    test: 0,
    _value: 0,
  },
  ch2Solenoids: {
    fuel: 0,
    air: 0,
    spanA: 0,
    spanB: 0,
    purge: 0,
    zero: 0,
    vacuum: 0,
    test: 0,
    _value: 0,
  },
  lhcGlowPlugAndHeadHeaterStatus: {
    headHeater1: 0,
    headHeater2: 0,
    glowPlug1: 0,
    glowPlug2: 0,
    lhc1: 0,
    lhc2: 0,
    _value: 0,
  },
  gainRangeAndHtStatus: {
    ht1: 0,
    ch1AmplificationStage: 0,
    ht2: 0,
    ch2AmplificationStage: 0,
    _value: 0,
  },
  userDigitalOutput: {
    isolated0: 0,
    isolated1: 0,
    isolated2: 0,
    isolated3: 0,
    nonIsolated0: 0,
    nonIsolated1: 0,
    nonIsolated2: 0,
    nonIsolated3: 0,
    nonIsolatedValue: 0,
    isolatedValue: 0,
    _value: 0,
  },
  ch1FidPressureSetpoint: 0,
  ch1CpPressureSetpoint: 0,
  pumpRpmSetpoint: 0,
  ch1FuelPressureSetpoint: 0,
  ch1AirPressureSetpoint: 0,
  glowplugCurrent: 0,
  ch2AirPressureSetpoint: 0,
  ch1ProbeTemperatureSetpoint: 0,
  ch1HeadTemperatureSetpoint: 0,
  ch2ProbeTemperatureSetpoint: 0,
  ch2HeadTemperatureSetpoint: 0,
  span1: 0,
  zero1: 0,
  span2: 0,
  zero2: 0,
};

export type HmPackage = DeepWriteable<typeof hmPackageInitial>;

export class Encoder extends Transform {
  constructor(options?: TransformOptions) {
    super({
      ...options,
      writableObjectMode: true,
    });
  }

  // eslint-disable-next-line @typescript-eslint/explicit-member-accessibility
  public _transform(
    chunk: HmPackage | HmPackage[],
    _encoding: BufferEncoding,
    callback: TransformCallback,
  ): void {
    const hmPackages = Array.isArray(chunk) ? chunk : [chunk];

    hmPackages.forEach((hmPackage) => {
      // TODO: Transform hmPackage to hmPacket instead of clonning it.
      merge(this._hmPacket, hmPackage);

      this._hmPacket.esn = FID50_ESN;

      this.push(this._hmPacketRaw);
    });

    callback();
  }

  readonly _hmPacket = new HmPacket();
  readonly _hmPacketRaw = HmPacket.raw(this._hmPacket);
}
