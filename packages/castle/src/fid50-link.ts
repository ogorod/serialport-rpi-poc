// Hey Emacs, this is -*- coding: utf-8 -*-

import { pipeline } from 'stream';
import type { Duplex } from 'stream';

import { BehaviorSubject, Subject } from 'rxjs';
import cloneDeep from 'lodash.clonedeep';

import SerialPort from 'serialport';

import { Encoder, hmPackageInitial } from '~/fid50-encoder';
import type { HmPackage } from '~/fid50-encoder';

import { Decoder, mhPackageInitial } from '~/fid50-decoder';
import type { MhPackage } from '~/fid50-decoder';

export class Fid50Link {
  constructor(
    readonly portPath = '/dev/ttyS0',
    readonly portBaudRate = 19200,
    readonly txIntervalMilliSecond = 100,
  ) {
    this._port = new SerialPort(
      this.portPath,
      { baudRate: this.portBaudRate },
      this.onOpenHandler,
    );
  }

  // /b/; behaviour
  // /b/{

  readonly txPackage$ = new BehaviorSubject(cloneDeep(hmPackageInitial));

  readonly connected$ = new BehaviorSubject(false);

  readonly sentHmPackage$ = new Subject<HmPackage>();
  readonly receivedHhPackage$ = new BehaviorSubject(
    cloneDeep(mhPackageInitial),
  );

  close = (): void => {
    if (this._txTimer) {
      clearInterval(this._txTimer);
    }
    if (this._port.isOpen) {
      this._port.close();
    }
  };

  // /b/}

  private _txTimer?: ReturnType<typeof setInterval>;

  private readonly txHandler = (): void => {
    if (!this.connected$.getValue()) {
      return;
    }

    const hmPackage = this.txPackage$.getValue();
    this._encoder.write(hmPackage);
    this.sentHmPackage$.next(hmPackage);
  };

  private readonly onOpenHandler: SerialPort.ErrorCallback = (
    portOpenError,
  ): void => {
    if (portOpenError) {
      console.error(`error while opening ${this.portPath}`);
    } else {
      pipeline(
        this._encoder,
        this._port as Duplex,
        this._decoder,
        (pipelineError) => {
          this.connected$.next(false);
          console.log('FID50 pipline closed', pipelineError);
        },
      );

      this._txTimer = setInterval(this.txHandler, this.txIntervalMilliSecond);

      this._decoder.on('data', (mhPackage: MhPackage) => {
        this.receivedHhPackage$.next(mhPackage);
        // console.log('Module-to-Host:', mhPackage);
      });

      this.connected$.next(true);
    }
  };

  private readonly _port: SerialPort;
  private readonly _decoder = new Decoder();
  private readonly _encoder = new Encoder();
}
