// Hey Emacs, this is -*- coding: utf-8 -*-

import { Fid50Link } from '~/fid50-link';

export default async function main(): Promise<void> {
  const fid50Link = new Fid50Link();

  // To set new data for sending and inform observers on txPackage change:
  // fid50Link.txPackage$.next(packageToSend)

  fid50Link.connected$.subscribe((connected) => {
    if (connected) {
      console.log('FID 50 connected');
    } else {
      console.log('FID 50 disconnected');
    }
  });

  fid50Link.sentHmPackage$.subscribe((hmPackage) => {
    console.log('Host-to-Module:', hmPackage);
  });

  fid50Link.receivedHhPackage$.subscribe((mhPackage) => {
    console.log('Module-to-Host:', mhPackage);
  });

  process.on('SIGINT', fid50Link.close);
  process.on('SIGTERM', fid50Link.close);
}
